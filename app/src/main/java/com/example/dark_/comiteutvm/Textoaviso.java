package com.example.dark_.comiteutvm;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Textoaviso extends Fragment {

    private EditText txt_avisoutvm;
    private TextView txtresponsable,txtpuesto;
    private Button btn_enviar;
    String Stxtaviso;
    String Stxtresponsable;
    String Shora_fecha;
    String Stresponsable;
    String Stxtpuesto;




    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saved) {
        View view = inflater.inflate(R.layout.avisoutvm, null);

        txt_avisoutvm = (EditText) view.findViewById(R.id.edt_avisoutvm);
        txtresponsable = (TextView) view.findViewById(R.id.txtemitioresponsable);
        txtpuesto = (TextView) view.findViewById(R.id.txtpuesto);
        btn_enviar = (Button) view.findViewById(R.id.btn_enviar);

        Stresponsable = "Ing. Erick Azael Viveros Gonzalez ";
        Stxtpuesto = "Administrador de app";


        txtresponsable.setText(Stresponsable);
        txtpuesto.setText(Stxtpuesto);

        Shora_fecha = getDateTime().toString();
        Stxtresponsable = txtresponsable.getText().toString();
        Stxtaviso = txt_avisoutvm.getText().toString();

        btn_enviar.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast toast2 = Toast.makeText(getView().getContext(), "AVISO ENVIADO \n", Toast.LENGTH_SHORT);
                        toast2.show();
                        Intent intent = new Intent(getView().getContext(), MainActivity.class);
                        startActivity(intent);
                    }
                });

        return view;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }



}
