package com.example.dark_.comiteutvm;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SqlDataHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "contactsManager.db";
    // Table Names
    private static final String TABLE_ALERTAS = "alertas";
    private static final String TABLE_USUARIOS = "usuarios";

    // Common column alertas
    private static final String KEY_idalerta = "idalerta";
    private static final String KEY_titulo_alerta="tituloalerta";
    private static final String KEY_contenido_alerta = "contenidoalerta";
    private static final String KEY_nombre_completo_emitio = "nombrecompletoemitio";
    private static final String KEY_cargo_emitio = "cargoemitio";
    private static final String KEY_fecha_alerta = "fechaalerta";
    private static final String KEY_tipo_alerta = "tipoalerta";
    private static final String KEY_estatus_alerta = "estatusalerta";

    // NOTES Table Generales
    private static final String KEY_idempleado = "idempleado";
    private static final String KEY_rfc = "rfc";
    private static final String KEY_nombre_completo_emite = "nombrecompletoemitio";
    private static final String KEY_cargo_emite = "cargoemite";
    private static final String KEY_tipo_usuario = "tipoasuario";
    private static final String KEY_tipo_app = "tipoapp";




    // Table Create alertas
    static final String CREATE_TABLE_ALERTAS ="CREATE TABLE "
            + TABLE_ALERTAS + "("
            + KEY_idalerta + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + KEY_titulo_alerta + " TEXT,"
            + KEY_contenido_alerta + " TEXT,"
            + KEY_nombre_completo_emitio + " TEXT,"
            + KEY_cargo_emitio + " TEXT,"
            + KEY_fecha_alerta + " TEXT,"
            + KEY_tipo_alerta + " TEXT,"
            + KEY_estatus_alerta + " TEXT)";


    // Table Create usuarios
    static final String CREATE_TABLE_USUARIOS ="CREATE TABLE "
            + TABLE_USUARIOS + "("
            + KEY_idempleado + " TEXT,"
            + KEY_rfc + " TEXT,"
            + KEY_nombre_completo_emite + " TEXT,"
            + KEY_cargo_emite + " TEXT,"
            + KEY_tipo_usuario + " TEXT,"
            + KEY_tipo_app + " TEXT)";


    public SqlDataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.d("BASE DE DATOS", "CREACION DE DE BASE DE DATOS");

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ALERTAS);
        db.execSQL(CREATE_TABLE_USUARIOS);
        //db.execSQL("INSERT INTO alertas VALUES (1,'EJEMPPLOTITULO','EJEMPLOCONTENIDOALERTA','EJMPLONOMBRE','EJEMPLOCARGO','12/12/12 6PM','INTRANET','ESTATUSALERTA')");


        Log.d("BASE DE DATOS", "Tablas de base");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS createUsuarios");

        db.execSQL("DROP TABLE IF EXISTS createAlertas");

        onCreate(db);
    }


    public List<DBUsuarios> getGenerales() {
        List<DBUsuarios> todos = new ArrayList<DBUsuarios>();
        String selectQuery = "SELECT  * FROM " + TABLE_USUARIOS;

        //Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                DBUsuarios td = new DBUsuarios();
                td.setIdempleado( c.getString( c.getColumnIndex(KEY_idempleado) ));
                td.setRfc( c.getString( c.getColumnIndex(KEY_rfc) ));
                td.setNombre_completo_emitio( c.getString( c.getColumnIndex(KEY_nombre_completo_emite)));
                td.setCargo_emite(c.getString(c.getColumnIndex(KEY_cargo_emite)));
                td.setTipo_usuario(c.getString(c.getColumnIndex(KEY_tipo_usuario)));
                td.setTipo_app(c.getString(c.getColumnIndex(KEY_tipo_app)));
                // adding to todo list
                todos.add(td);
            } while (c.moveToNext());
        }

        return todos;
    }

    /**
     * Eliminar el registro con el identificador indicado
     */


    public long createUsuarios(DBUsuarios tag) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues values = new ContentValues();

        values.put(KEY_idempleado, tag.getIdempleado());
        values.put(KEY_rfc, tag.getRfc());
        values.put(KEY_nombre_completo_emite, tag.getNombre_completo_emitio());
        values.put(KEY_cargo_emite, tag.getCargo_emite());
        values.put(KEY_tipo_usuario, tag.getTipo_usuario());
        values.put(KEY_tipo_app, tag.getTipo_app());

        // insert row
        Log.i("datos recibi",values.toString());
        long tag_id = db.insert(TABLE_USUARIOS, null, values);

        Log.i("datos recibi",db.toString());
        Log.i("datos recibi","datos insertados");
        return tag_id;
    }
    public int getToDoCount() {
        String countQuery = "SELECT  * FROM " + TABLE_USUARIOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    public List<DBUsuarios> getUsuarioValido() {
        List<DBUsuarios> todos = new ArrayList<DBUsuarios>();
        String selectQuery = "SELECT  * FROM " + TABLE_USUARIOS;

        //Log.e(LOG, selectQuery);

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (c.moveToFirst()) {
            do {
                DBUsuarios td = new DBUsuarios();
                td.setIdempleado(c.getString(c.getColumnIndex(KEY_idempleado)));
                td.setNombre_completo_emitio(c.getString(c.getColumnIndex(KEY_nombre_completo_emite)));
                td.setCargo_emite(c.getString(c.getColumnIndex(KEY_cargo_emite)));


                // adding to todo list
                todos.add(td);
            } while (c.moveToNext());
        }

        return todos;
    }





    public long createAlertas(DBAlertas tag) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_idalerta, tag.getIdalerta());
        values.put(KEY_titulo_alerta, tag.getTitulo_alerta());
        values.put(KEY_contenido_alerta,tag.getContenido_alerta());
        values.put(KEY_nombre_completo_emitio, tag.getNombre_completo_emitio());
        values.put(KEY_cargo_emitio, tag.getCargo_emitio());
        values.put(KEY_fecha_alerta, tag.getFecha_alerta());
        values.put(KEY_tipo_alerta, tag.getTipo_alerta());
        values.put(KEY_estatus_alerta, tag.getEstatus_alerta());
        // insert row
        Log.i("datos recibi","datitos");
        long tag_id = db.insert(TABLE_ALERTAS, null, values);

        return tag_id;
    }



    public void updateusuarios(String rfc) {
        SQLiteDatabase db = this.getReadableDatabase();
        //Establecemos los campos-valores a actualizar
        ContentValues valores = new ContentValues();
        valores.put("tipoapp","1");
        //Actualizamos el registro en la base de datos
        db.update("usuarios", valores, "rfc="+rfc, null);
    }

}

