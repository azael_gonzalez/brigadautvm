package com.example.dark_.comiteutvm;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class VerifUusarioperfil extends AppCompatActivity {
    ArrayList<DBUsuarios> listaAlertas;
    EditText edirfc;
    Button btnsiguiente;

    TextView txtpuestov;
    TextView txtnombrev;
    CheckBox checkBoxtodov;
    CheckBox checkBoxavisov;
    CheckBox checkBoxsismov;

    String respuestacheck,noempleado ,tipoapp;
    //importar usuaarios
    ProgressDialog progress;
    SqlDataHelper conn;
    String url_rutas;
    String url_rutas2;
    String tipo1="1";
    String tipo2="2";
    String tipo3="3";
    String usuarioconsulta,puestoconsulta;
    //
    String ValidarRFC;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.perfil);

        btnsiguiente = (Button) findViewById(R.id.btnsiguiente);

        txtpuestov= (TextView) findViewById(R.id.txtpuesto);
        txtnombrev = (TextView) findViewById(R.id.txtnombre);

        checkBoxavisov= (CheckBox) findViewById(R.id.checkBoxaviso);
        checkBoxsismov= (CheckBox) findViewById(R.id.checkBoxsismo);
        edirfc= (EditText) findViewById(R.id.edirfc);



        conn=new SqlDataHelper(getApplicationContext());

        url_rutas = "http://www.utvm.edu.mx/webservice/BuscarUsuario.php?rfc=";
        url_rutas2 = "http://10.100.96.26/webservice/BuscarUsuario.php?rfc=";

        consultarusuario2();



        btnsiguiente.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {

                        Intent intent = new Intent(getApplication(), MainActivity.class);
                        startActivity(intent);
                        finish();
                        //codigo


                    }
                });
    }

    private void borrar() {
        /**
         * Borramos el registro con confirmación
         */
        AlertDialog.Builder dialogEliminar = new AlertDialog.Builder(this);

        dialogEliminar.setIcon(android.R.drawable.ic_dialog_alert);
        dialogEliminar.setTitle(getResources().getString(R.string.eliminar_titulo));
        dialogEliminar.setMessage(getResources().getString(R.string.eliminar_mensaje));
        dialogEliminar.setCancelable(false);

        dialogEliminar.setPositiveButton(getResources().getString(android.R.string.ok), new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int boton) {

                String selectQuery = "DELETE FROM usuarios ";
                Cursor cursor = conn.getReadableDatabase().rawQuery(selectQuery, null);

                mostrarAlerta(getResources().getString(R.string.eliminar_mensaje));
                startActivity(new Intent(VerifUusarioperfil.this, WelcomeActivity.class));
                finish();

                /**
                 * Devolvemos el control
                 */
                setResult(RESULT_OK);
                finish();
            }
        });

        dialogEliminar.setNegativeButton(android.R.string.no, null);

        dialogEliminar.show();

    }


    public void mostrarAlerta(String mensaje) {
        Toast toast = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void consultarusuario2(){

        String tipousu="";
        ///////////---------------------------
        conn = new SqlDataHelper(getApplicationContext());
        List<DBUsuarios> allTags = conn.getGenerales();
        try {
            for (DBUsuarios todo : allTags) {
                edirfc.setText(todo.getRfc());
                txtnombrev.setText(todo.getNombre_completo_emitio());
                txtpuestov.setText(todo.getCargo_emite());
                puestoconsulta=(todo.getCargo_emite());
                tipousu=(todo.getTipo_usuario());
                tipousuario(tipousu);

                mostrarAlerta("Usuario Actual: "+txtnombrev.getText().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ////////////---------------------------
    }

    public void consultarusuario(){
        ///////////---------------------------
        conn = new SqlDataHelper(getApplicationContext());
        List<DBUsuarios> allTags = conn.getGenerales();
        try {
            for (DBUsuarios todo : allTags) {
                usuarioconsulta=(todo.getNombre_completo_emitio());
                puestoconsulta=(todo.getCargo_emite());
                mostrarAlerta("usuario: "+usuarioconsulta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ////////////---------------------------
    }

    public void EnviarRecibirDatos(String URL){
        Log.d("AQUERY", "inicio");
        AQuery aq = new AQuery(getApplicationContext());

        listaAlertas = new ArrayList<>();
        aq.progress(1).ajax(URL, JSONObject.class, new AjaxCallback<JSONObject>(){
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);

                try {
                    JSONArray notificacion = object.getJSONArray("usuario");

                    for (int i = 0; i < notificacion.length(); i++){
                        JSONObject datos = notificacion.getJSONObject(i);
                        DBUsuarios alertas = new DBUsuarios();
                        Log.d("AQUERY", "bdusuarios");
                        String idempleado = datos.getString("idempleado");
                        String rfc = datos.getString("rfc");
                        String cargo_emite = datos.getString("cargo_emite");
                        String tipo_usuario = datos.getString("tipo_usuario");
                        String nombre_completo_emitio = datos.getString("nombre_completo_emitio");
                        String tipo_app = datos.getString("tipo_app");

                        alertas.setIdempleado(idempleado);
                        alertas.setRfc(rfc);
                        alertas.setCargo_emite(cargo_emite);
                        alertas.setTipo_usuario(tipo_usuario);
                        alertas.setNombre_completo_emitio(nombre_completo_emitio);
                        alertas.setTipo_app(tipo_app);

                        listaAlertas.add(alertas);

                        txtpuestov.setText(listaAlertas.get(0).getCargo_emite());
                        noempleado =(listaAlertas.get(0).getIdempleado());
                        txtnombrev.setText(listaAlertas.get(0).getNombre_completo_emitio());
                        respuestacheck = (listaAlertas.get(0).getTipo_usuario());
                        tipoapp=(listaAlertas.get(0).getTipo_app());
                        tipousuario(respuestacheck);
                        Log.d("AQUERY", listaAlertas.toString());
                    }
                }
                catch (JSONException e){
                    mostrarAlerta("USUARIO NO REGISTRADO");
                    Log.e("JSON ERROR: ", e.getLocalizedMessage());
                }

                mostrarAlerta("USUARIO guardado");
                DBUsuarios nuevo_usuario = new DBUsuarios(
                        noempleado,
                        edirfc.getText().toString(),
                        txtpuestov.getText().toString(),
                        respuestacheck,
                        txtnombrev.getText().toString(),
                        tipoapp);
                conn.createUsuarios(nuevo_usuario);
                conn.close();
            }
        });
    }

    public void tipousuario(String checl){
        ////validacion

        if (checl.equals(tipo1)) {
            mostrarAlerta("Tipo usuario: "+respuestacheck);
            checkBoxavisov.setChecked(true);
            checkBoxsismov.setChecked(false);
            btnsiguiente.setVisibility(View.VISIBLE);
        } else if (checl.equals (tipo2)) {
            mostrarAlerta("Tipo usuario: "+respuestacheck);
            checkBoxavisov.setChecked(true);
            checkBoxsismov.setChecked(true);
            btnsiguiente.setVisibility(View.VISIBLE);
        } else if ((checl .equals (tipo3))) {
            mostrarAlerta("Tipo usuario: "+respuestacheck);
            checkBoxavisov.setChecked(false);
            checkBoxsismov.setChecked(true);
            btnsiguiente.setVisibility(View.VISIBLE);
        } else {
            mostrarAlerta("El usuario no cumple con los permisos");
            checkBoxavisov.setChecked(false);
            checkBoxsismov.setChecked(false);
            btnsiguiente.setVisibility(View.GONE);
        }
    }





}
