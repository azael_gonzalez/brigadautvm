package com.example.dark_.comiteutvm;

import android.app.Fragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class UsuarioFragment extends Fragment  {
    private ImageView sismos;
    private ImageView avisos;
    private View view;

    public static UsuarioFragment newInstance() {
        return new UsuarioFragment();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        view = inflater.inflate(R.layout.mainfragment, null);


        //codigo
        sismos = (ImageView) view.findViewById(R.id.btnsismo);
        //sismos.setOnClickListener(this);
        sismos.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast toast1 = Toast.makeText(view.getContext(),"SISMO  "+getDateTime().toString(), Toast.LENGTH_SHORT);
                        toast1.show();


                    }
                });

        avisos = (ImageView) view.findViewById(R.id.btnaviso);
        avisos.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                        Toast toast2 = Toast.makeText(view.getContext(),"AVISO"+getDateTime().toString(), Toast.LENGTH_SHORT);
                        toast2.show();
                    }
                });

        return view;

    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
        }


}
