package com.example.dark_.comiteutvm;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //SqlDataHelper dbn;
    //frameLayout
    String tipo1="1";
    String tipo2="2";
    String tipo3="3";
    SqlDataHelper conn;
    private FrameLayout frameLayout;
    private FragmentManager manager;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

      //  dbn = new SqlDataHelper(getApplicationContext());

        //fragmento visualizar uno con vista padre
        frameLayout = (FrameLayout) findViewById(R.id.framefrag);
        manager = getFragmentManager();
        transaction = manager.beginTransaction();

        consultarusuario2();
        //MainFragment mainFragment = new MainFragment();
        //transaction.add(frameLayout.getId(), mainFragment).commit();

        //boton flotante


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Manejar los clics del elemento de la barra de acción aquí.
        // La barra de acciones gestionará automáticamente
        // los clics en el botón Inicio / Arriba, siempre que
        // especifique una actividad principal en AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {

            //PrefManager prefManager = new PrefManager(getApplicationContext());
            // hacer el lanzamiento por primera vez VERDADERO
            //prefManager.setFirstTimeLaunch(true);
            startActivity(new Intent(MainActivity.this, VerifUusarioperfil.class));
            finish();
            ///boton de configuracion
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        manager = getFragmentManager();
        transaction = manager.beginTransaction();

        if (id == R.id.nav_slideshow) {
            //fragmentos
            InstruccionesFragment instruccionesFragment = new InstruccionesFragment();
            transaction.replace(frameLayout.getId(), instruccionesFragment).commit();
            // Handle the camera action

        } else if (id == R.id.inicio) {

            consultarusuario2();

        } else if (id == R.id.nav_manage) {
            AvisosFragment avisosFragment = new AvisosFragment();
            transaction.replace(frameLayout.getId(), avisosFragment).commit();


        } else if (id == R.id.nav_send) {
            CreditosFragment creditosFragment = new CreditosFragment();
            transaction.replace(frameLayout.getId(), creditosFragment).commit();




        } else if (id == R.id.huevo) {
            ManagerFragment managerFragment = new ManagerFragment();
            transaction.replace(frameLayout.getId(), managerFragment).commit();


        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    public void activarServicio() {
        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(this, MyServiceNotificacion.class);
        startService(service);

        // Definimos la accion de la pulsacion sobre la notificacion (esto es opcional)
        Context context = getApplicationContext();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    }

    public void activarServicio2() {
        Log.d("MainActivity", "activarServicio 2()");
        Intent service2 = new Intent(this, ServicioNotificacion.class);
        startService(service2);

        // Definimos la accion de la pulsacion sobre la notificacion (esto es opcional)
        Context context = getApplicationContext();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
    }



    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
    public void mostrarAlerta(String mensaje) {
        Toast toast = Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT);
        toast.show();
    }

    public void consultarusuario2(){

        String tipousu="";
        ///////////---------------------------
        conn = new SqlDataHelper(getApplicationContext());
        List<DBUsuarios> allTags = conn.getGenerales();
        try {
            for (DBUsuarios todo : allTags) {

                tipousu=(todo.getTipo_usuario());
                tipousuario(tipousu);

                mostrarAlerta("Usuario Actual: "+tipousu);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        ////////////---------------------------
    }

    public void tipousuario(String checl){
        ////validacion

        if (checl.equals(tipo1)) {
            MainFragmentaviso mainFragment1 = new MainFragmentaviso();
            transaction.add(frameLayout.getId(), mainFragment1).commit();
        } else if (checl.equals (tipo2)) {
            MainFragment mainFragment = new MainFragment();
            transaction.add(frameLayout.getId(), mainFragment).commit();
        } else if ((checl .equals (tipo3))) {

            MainFragmentsismo mainFragment2 = new MainFragmentsismo();
            transaction.add(frameLayout.getId(), mainFragment2).commit();
        } else {
            mostrarAlerta("El usuario no cumple con los permisos");

        }
    }
}
