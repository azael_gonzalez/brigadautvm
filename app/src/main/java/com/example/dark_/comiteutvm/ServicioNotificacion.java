package com.example.dark_.comiteutvm;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class ServicioNotificacion extends Service {
    private static final String TAG = "MyService";


    String textoalerta = "AVISO UTVM !!";
    String textotituloalerta = "Aviso de la intranet !! ";

    @Override
    public IBinder onBind(Intent i) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {

        new Thread(new Runnable() {

            @Override
            public void run() {
                Log.d(TAG, "Incio de servicio de notificación");
                // El servicio se finaliza a si mismo cuando finaliza su
                // trabajo.
                try {
                    // Simulamos trabajo de 1 segundo.
                    Thread.sleep(5000);

                    // Instanciamos e inicializamos nuestro manager.
                    NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                    Uri uri = Uri.parse(String.format("android.resource://%s/%s/%s", getApplication().getPackageName(), "raw", "avisoutvm"));


                    NotificationCompat.Builder avisoutvm = new NotificationCompat.Builder(
                            getBaseContext())
                            .setSmallIcon(R.drawable.logo)
                            .setContentTitle(textotituloalerta)
                            .setContentText(textoalerta)
                            //.setContentIntent(pendingIntent)
                            .setWhen(System.currentTimeMillis())
                            .setSound(uri)
                            .setVibrate(new long[]{100, 1000, 100, 2000})
                            .setAutoCancel(true);


                    nManager.notify(123456, avisoutvm.build());


                    Log.d(TAG, "sleep finished");
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

            }
        }).start();

        this.stopSelf();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        Log.d(TAG, "FirstService destroyed");
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}

