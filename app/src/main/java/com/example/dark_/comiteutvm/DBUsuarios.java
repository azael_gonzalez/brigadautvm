package com.example.dark_.comiteutvm;

public class DBUsuarios {
    String idempleado;
    String rfc;
    String cargo_emite;
    String tipo_usuario;
    String nombre_completo_emitio;
    String tipo_app;

    public DBUsuarios() {
        this.idempleado = idempleado;
        this.rfc = rfc;
        this.cargo_emite = cargo_emite;
        this.tipo_usuario = tipo_usuario;
        this.nombre_completo_emitio = nombre_completo_emitio;
        this.tipo_app = tipo_app;
    }

    public DBUsuarios(String idempleado, String rfc, String cargo_emite, String tipo_usuario, String nombre_completo_emitio, String tipo_app) {
        this.idempleado = idempleado;
        this.rfc = rfc;
        this.cargo_emite = cargo_emite;
        this.tipo_usuario = tipo_usuario;
        this.nombre_completo_emitio = nombre_completo_emitio;
        this.tipo_app = tipo_app;
    }

    public String getIdempleado() {
        return idempleado;
    }

    public void setIdempleado(String idempleado) {
        this.idempleado = idempleado;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getCargo_emite() {
        return cargo_emite;
    }

    public void setCargo_emite(String cargo_emite) {
        this.cargo_emite = cargo_emite;
    }

    public String getTipo_usuario() {
        return tipo_usuario;
    }

    public void setTipo_usuario(String tipo_usuario) {
        this.tipo_usuario = tipo_usuario;
    }

    public String getNombre_completo_emitio() {
        return nombre_completo_emitio;
    }

    public void setNombre_completo_emitio(String nombre_completo_emitio) {
        this.nombre_completo_emitio = nombre_completo_emitio;
    }

    public String getTipo_app() {
        return tipo_app;
    }

    public void setTipo_app(String tipo_app) {
        this.tipo_app = tipo_app;
    }
}
