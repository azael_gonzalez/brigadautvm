package com.example.dark_.comiteutvm;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link MainFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainFragment extends Fragment implements Response.Listener<JSONObject>,Response.ErrorListener, OnFragmentInteractionListener{
    SqlDataHelper conn;
    private ImageView alerta;
    private ImageView sismo;
    String usuario , puesto;
    String usuarioconsulta,puestoconsulta;
    String tipousu="0";
    String tipo1="1";
    String tipo2="2";
    String tipo3="3";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    String direccionutvm;
    String direccionutvm2;
    private OnFragmentInteractionListener mListener;

    ProgressDialog progreso;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    public MainFragment() {
        // Required empty public constructor
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PrincipalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MainFragment newInstance(String param1, String param2) {
        MainFragment fragment = new MainFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }



    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState){
        View view = inflater.inflate(R.layout.mainfragment,container, false);
        alerta = (ImageView) view.findViewById(R.id.btnaviso);
        sismo = (ImageView) view.findViewById(R.id.btnsismo);

        request = Volley.newRequestQueue(view.getContext());
       // conn = new SqlDataHelper(view.getContext().getApplicationContext());
        conn = new SqlDataHelper(view.getContext().getApplicationContext());

        List<DBUsuarios> allTags = conn.getGenerales();
        try {
            for (DBUsuarios todo : allTags) {
                usuarioconsulta=(todo.getNombre_completo_emitio());
                puestoconsulta=(todo.getCargo_emite());
                mostrarAlerta("usuario: "+usuarioconsulta);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        //consultarusuario();
        sismo.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {
                    //activarServicio();
                        Toast toast2 = Toast.makeText(getView().getContext(), "SISMO !! \n", Toast.LENGTH_SHORT);
                        toast2.show();

                        mensajedialogo();
                    }
                });

        alerta.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View view) {

                        new DialogoPersonalizado().show(getFragmentManager(), "DialogoPersonalizado");
                    }
                });


        return view;


    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

@Override
public void onAttach(Context context) {
    super.onAttach(context);
    if (this instanceof OnFragmentInteractionListener) {
        mListener = (OnFragmentInteractionListener) this;
    } else {
        throw new RuntimeException(context.toString()
                + " must implement OnFragmentInteractionListener");
    }
}

    public void activarServicio() {
        Toast toast2 = Toast.makeText(getView().getContext(), "SISMO !! \n", Toast.LENGTH_SHORT);
        toast2.show();

        Log.d("MainActivity", "activarServicio()");
        Intent service = new Intent(getView().getContext(), MyServiceNotificacion.class);
        getActivity().startService(service);
    }

    public void activarServicio2() {
        Toast toast2 = Toast.makeText(getView().getContext(), "AVISO UTVM !!\n", Toast.LENGTH_SHORT);
        toast2.show();

        Log.d("MainActivity", "activarServicio 2()");
        Intent service2 = new Intent(getView().getContext(), ServicioNotificacion.class);
        getActivity().startService(service2);
    }

    private void mensajedialogo(){
        AlertDialog.Builder builder = new AlertDialog.Builder(getView().getContext());
        builder.setMessage("¿Desea mandar un aviso de Sismo?")
                .setTitle("Advertencia")
                .setCancelable(false)
                .setNegativeButton("Cancelar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        })
                .setPositiveButton("Continuar",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                cargarservicio(); // metodo que se debe implementar

                                Toast toast5 = Toast.makeText(getView().getContext(),"AVISO ENVIADO A TODOS ", Toast.LENGTH_SHORT);
                                toast5.show();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }



    private void cargarservicio(){
        progreso = new ProgressDialog(getView().getContext());
        progreso.setMessage("Cargando...");
        progreso.show();
        Log.d("MainActivity", "servicio de json");

///direcciones con pruebas
        String direccion = "http://10.100.96.26/webservice/AlertaSismo.php?nombre=?&cargo=?";
        direccionutvm="http://www.utvm.edu.mx";
        direccionutvm2="http://10.100.96.26";
        String nombre= usuario;
        String puesto="desarrollo";
        String url2= direccionutvm+"/webservice/AlertaSismo.php?nombre="+usuarioconsulta+"&cargo="+puestoconsulta;
        Log.d("MainActivity", url2);
        String url= "http://10.100.96.26/webservice/AlertaSismo.php";
///
        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url2,null,this,this);
        request.add(jsonObjectRequest);

        Log.d("MainActivity", "datos de json");
    }


    @Override
    public void onErrorResponse(VolleyError error) {
        progreso.hide();
        Toast.makeText(getView().getContext(),"Error al registrar" +error.toString(),Toast.LENGTH_SHORT);
        Log.i("ERROR",error.toString());
    }


    @Override
    public void onResponse(JSONObject response) {
        Toast.makeText(getView().getContext(),"Se Registro Correctamente",Toast.LENGTH_SHORT);
        progreso.hide();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
    public void mostrarAlerta(String mensaje) {
        Toast toast = Toast.makeText(getView().getContext(), mensaje, Toast.LENGTH_SHORT);
        toast.show();

        ////////////---------------------------
    }



}
