package com.example.dark_.comiteutvm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

import java.util.List;

public class DialogoPersonalizado extends DialogFragment implements Response.Listener<JSONObject>,Response.ErrorListener, OnFragmentInteractionListener {

    private EditText txt_avisoutvm, edtTitulo;
    private TextView txtresponsable, txtpuesto;
    private Button btn_enviar;
    String SedtTitulo;
    String Stxtaviso;
    String Stxtresponsable;
    String Stresponsable;
    String cargo;
    String direccionutvm;
    String direccionutvm2;
    SqlDataHelper conn;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;




    private OnFragmentInteractionListener mListener;

    ProgressDialog progreso;

    RequestQueue request;
    JsonObjectRequest jsonObjectRequest;

    public DialogoPersonalizado() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PrincipalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DialogoPersonalizado newInstance(String param1, String param2) {
        DialogoPersonalizado fragment = new DialogoPersonalizado();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.avisoutvm, null);
        builder.setView(view);

        conn=new SqlDataHelper(view.getContext().getApplicationContext());

        //Stresponsable = "Ing. Erick Azael Viveros Gonzalez ";
        edtTitulo = (EditText) view.findViewById(R.id.edtTitulo);
        txt_avisoutvm = (EditText) view.findViewById(R.id.edt_avisoutvm);
        txtresponsable = (TextView) view.findViewById(R.id.txtemitioresponsable);
        btn_enviar = (Button) view.findViewById(R.id.btn_enviar);



        try {
            //si existe un usuario poner datos

            List<DBUsuarios> allTags = conn.getGenerales();
            if( allTags.size() > 0 ){
                for (DBUsuarios todo : allTags) {


                    txtresponsable.setText(todo.getNombre_completo_emitio());
                    cargo=todo.getCargo_emite();
                    String checkdatosrespon = txtresponsable.getText().toString();
                    mostrarAlerta("usuario: "+checkdatosrespon);


                }
            }else{
                mostrarAlerta("Usuario no encontrado");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



        //txtresponsable.setText(Stresponsable.toString());

        request = Volley.newRequestQueue(view.getContext());
        btn_enviar.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(txtresponsable.getText().toString().equals("Vacio")){
                            mostrarAlerta("Responsable no valido");

                        }else if(txt_avisoutvm.getText().toString().equals("")){
                            mostrarAlerta("No existe cuerpo de aviso");

                        }else if( edtTitulo.getText().toString().equals("")){
                            mostrarAlerta("El titulo esta vacio");
                        }else{
                            cargarserviciopersonalizado();
                            mostrarAlerta("AVISO UTVM ENVIADO A TODOS");
                        dismiss();
                    }
                  }
                }
        );



        return builder.create();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (this instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) this;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    public void mostrarAlerta(String mensaje) {
        Toast toast = Toast.makeText(getActivity(), mensaje, Toast.LENGTH_SHORT);
        //toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }

    private void cargarserviciopersonalizado(){
        //progreso = new ProgressDialog(getView().getContext());
        //progreso.setMessage("Cargando...");
        //progreso.show();

        ///direcciones con pruebas
        direccionutvm="http://www.utvm.edu.mx";
        direccionutvm2="http://10.100.96.26";
        //

        SedtTitulo=edtTitulo.getText().toString();
        Stxtaviso=txt_avisoutvm.getText().toString();
        Stxtresponsable= txtresponsable.getText().toString();
        //cargo="Docente";

        String urldi="http://10.100.96.26/webservice/RegistrarAviso.php?titulo=?&contenido=?&nombre=?&cargo=?";
        String url3= direccionutvm+"/webservice/RegistrarAviso.php?titulo="+SedtTitulo+"&contenido="+Stxtaviso+"&nombre="+Stxtresponsable+"&cargo="+cargo;

        url3=url3.replace(" ", "%20");

        jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,url3,null,this,this);
        request.add(jsonObjectRequest);
    }
    @Override
    public void onErrorResponse(VolleyError error) {
       // progreso.hide();
        Toast.makeText(getView().getContext(),"Error al registrar" +error.toString(),Toast.LENGTH_SHORT);
        Log.i("ERROR",error.toString());
    }

    @Override
    public void onResponse(JSONObject response) {

        Log.i("REGISTRO","Se Registro Correctamente");
        // Toast.makeText(getView().getContext(),"Se Registro Correctamente",Toast.LENGTH_SHORT);
       // progreso.hide();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
