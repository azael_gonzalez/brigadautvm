package com.example.dark_.comiteutvm;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by erickpc on 16/10/2015.
 */
public class Backup {
    private final String currentDBPath = "/data/com.example.dark_/databases/contactsManager";
    private final String backupDBPath = "ALERTASUTVM/contactsManager";
    Context context;
    public Backup(){}
    Date fecha = new Date();
    public boolean BackUpExport() {
        boolean response;
        Date date = new Date();
        File sd = Environment.getExternalStorageDirectory();
        File data = Environment.getDataDirectory();
        FileChannel src=null;
        FileChannel dst=null;
        
        File currentDB = new File(data, currentDBPath);
        File backupDB = new File(sd, backupDBPath+"-"+getDateTime().toString());
        try {
            src = new FileInputStream(currentDB).getChannel();
            dst = new FileOutputStream(backupDB).getChannel();
            dst.transferFrom(src, 0, src.size());
            src.close();
            dst.close();
            response=true;
            //Toast.makeText(context, "Respaldo Local realizado", Toast.LENGTH_SHORT).show();
            if (currentDB.exists()) {}
            if (sd.canWrite()) {}
        } catch (Exception e) {
            response=false;
            //Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
        }
        return response;
    }
    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

}
